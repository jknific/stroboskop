# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://jknific@bitbucket.org/jknific/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/jknific/stroboskop/commits/71a230d5321efec354d7888b0b4e253f4e48a4e0

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/jknific/stroboskop/commits/5b89e4ad0d636d0e8f06294e23169c2d8c096fd5

Naloga 6.3.2:
https://bitbucket.org/jknific/stroboskop/commits/3ec6aeb1aa7693fe7b05ba19991e2e8472da5131

Naloga 6.3.3:
https://bitbucket.org/jknific/stroboskop/commits/86a80aa07cc023c6fed7b54a8a47de944ff7ea23

Naloga 6.3.4:
https://bitbucket.org/jknific/stroboskop/commits/6d88662f65e68a93f4fc74ed231ac511ff394a11

Naloga 6.3.5:
```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/jknific/stroboskop/commits/581a748852df245acea46f33cdf846964230abc2

Naloga 6.4.2:
https://bitbucket.org/jknific/stroboskop/commits/6393a553b914bc3bc07e8196c8ca89b67298d286

Naloga 6.4.3:
https://bitbucket.org/jknific/stroboskop/commits/cd224752420964723ccd9a8929c2601224c63161

Naloga 6.4.4:
https://bitbucket.org/jknific/stroboskop/commits/61c0f22922dae24b7061b93fe503d58abcc0b8e3